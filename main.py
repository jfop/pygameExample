import pygame
import random
import os,sys

import multiprocessing
import queue
import time
#compare the location spot to a random location next to it of a different color. Each color the location share of their own color +x to result. Roll 6 sided "die" then add +x and the higher result wins

#Creates constants
WIDTH = 640
HEIGHT = 480
PROCESSES = multiprocessing.cpu_count()


#Creates canvas(BOARD)
canvas = pygame.display.set_mode((WIDTH,HEIGHT))

#Creates array in array of every spot and lists each location true/false if it has similar surrounding pixels
checkedPixels = [[False for col in range(HEIGHT)] for row in range(WIDTH)]

#Creates queue to single to clock process(function) when a change is made
myQueue = Queue.Queue()


#Starts init
def StartGame():
    pygame.init()
    CreateCanvas()
    
#Creates multiple processes to run concurrently
def CreateProcesses():
    jobs = []
 #its - 1 to make room for a different process that (Look below)
    for x in range(PROCESSES - 1):
        p = multiprocessing.Process(target=RunGame)
        jobs.append(p)
        p.start()        

 #Run a completely seperate process that calculates changes per sec with remaining cpu core
    p = multiprocessing.Process(target=Clock)
    jobs.append(p)
    p.start()        

#Calculates changes per sec
def Clock():
    totalPerSec = 0.0
    while True:
        now = time.time()
        future = now + 1
        while time.time() < future:
            totalPerSec = totalPerSec + myQueue.get()
        print totalPerSec, " per sec"
        totalPerSec = 0
        
	#clock = pygame.time.Clock()
    #while True:
     #   print(clock.get_fps())
      #  clock.tick(1000)


#Creates board
def CreateCanvas():
	for a in range(WIDTH):
		for b in range(HEIGHT):
			randColor = random.randint(1,6)
			if randColor == 1:
				pygame.draw.rect(canvas, (255,255,0), (a, b, 1, 1))
			if randColor == 2:
				pygame.draw.rect(canvas, (0,0,0), (a, b, 1, 1))
			if randColor == 3:
				pygame.draw.rect(canvas, (255,16,0), (a, b, 1, 1))
			if randColor == 4:
				pygame.draw.rect(canvas, (22,0,88), (a, b, 1, 1))
			if randColor == 5:
				pygame.draw.rect(canvas, (98,34,77), (a, b, 1, 1))
			if randColor == 6:
				pygame.draw.rect(canvas, (255,0,255), (a, b, 1, 1))
	CheckPixels()


#Checks ENTIRE surrounding pixels and updates array after creating board, ran only once
def CheckPixels():
	for a in range(WIDTH):
		for b in range(HEIGHT):
			myColor = canvas.get_at((a,b))
			if a != 0:
				if myColor == canvas.get_at(((a-1),b)):
					checkedPixels[a][b] = True
			if b != 0:
				if myColor == canvas.get_at(((a),b-1)):
					checkedPixels[a][b] = True
			if a != WIDTH - 1:
				if myColor == canvas.get_at(((a+1),b)):
					checkedPixels[a][b] = True
			if b != HEIGHT - 1:
				if myColor == canvas.get_at(((a),b+1)):
					checkedPixels[a][b] = True
			#print(checkedPixels[a][b])
	CreateProcesses()

#CHECKS surrounding pixels to see if they share there are pixels not the same color and updates
def CheckPixel(a,b):
	myColor = canvas.get_at((a,b))
	checkedPixels[a][b] = False
	if a != 0:
		if myColor == canvas.get_at(((a-1),b)):
			checkedPixels[a][b] = True
	if b != 0:
		if myColor == canvas.get_at(((a),b-1)):
			checkedPixels[a][b] = True
	if a != WIDTH - 1:
		if myColor == canvas.get_at(((a+1),b)):
			checkedPixels[a][b] = True
	if b != HEIGHT - 1:
		if myColor == canvas.get_at(((a),b+1)):
			checkedPixels[a][b] = True
			#print(checkedPixels[a][b])
			
			
#COUNTS pixels that have the same color around them
def CheckSurroundingPixels(a,b):
	myColor = canvas.get_at((a,b))
	sharedPixels = 0
	if a != 0:
		if myColor == canvas.get_at(((a-1),b)):
			sharedPixels = sharedPixels + 1
	if b != 0:
		if myColor == canvas.get_at(((a),b-1)):
			sharedPixels = sharedPixels + 1
	if a != WIDTH - 1:
		if myColor == canvas.get_at(((a+1),b)):
			sharedPixels = sharedPixels + 1
	if b != HEIGHT - 1:
		if myColor == canvas.get_at(((a),b+1)):
			sharedPixels = sharedPixels + 1
	return sharedPixels

#Changes pixel color that loses
def PixelChange():
	randA = random.randint(0,WIDTH - 1)
	randB = random.randint(0,HEIGHT - 1)
	otherColor = 0
	otherColorCordsA = 0
	otherColorCordsB = 0
	if checkedPixels[randA][randB] == True:
		myColor = canvas.get_at((randA,randB))
		startingColorNumber = CheckSurroundingPixels(randA,randB)
		if randA != 0 and otherColor == 0:
			if myColor != canvas.get_at(((randA-1),randB)):
				otherColor = canvas.get_at(((randA-1),randB))
				otherColorCordsA = randA - 1
				otherColorCordsB = randB
		if randB != 0 and otherColor == 0:
			if myColor != canvas.get_at(((randA),randB-1)):
				otherColor = canvas.get_at(((randA),randB-1))
				otherColorCordsA = randA
				otherColorCordsB = randB - 1
		if randA != WIDTH - 1 and otherColor == 0:
			if myColor != canvas.get_at(((randA+1),randB)):
				otherColor = canvas.get_at(((randA+1),randB))
				otherColorCordsA = randA + 1
				otherColorCordsB = randB
		if randB != HEIGHT - 1 and otherColor == 0:
			if myColor != canvas.get_at(((randA),randB+1)):
				otherColor = canvas.get_at(((randA),randB+1))
				otherColorCordsA = randA
				otherColorCordsB = randB + 1
	
		sharedPixelsA = CheckSurroundingPixels(randA,randB)
		sharedPixelsB = CheckSurroundingPixels(otherColorCordsA,otherColorCordsB)
		randRollA = random.randint(1,6)
		randRollB = random.randint(1,6)
		sharedPixelsA = sharedPixelsA + randRollA
		sharedPixelsB = sharedPixelsB + randRollB
		if sharedPixelsA > sharedPixelsB:
			canvas.set_at((otherColorCordsA,otherColorCordsB), myColor)
			CheckPixel(otherColorCordsA,otherColorCordsB)
		else:
			canvas.set_at((randA,randB), otherColor)
			CheckPixel(randA,randB)
        myQueue.put(1)
#infinite loop, keeps game running	
def RunGame():
	gamerunning = True
	while gamerunning:
		PixelChange()
		pygame.display.update()

StartGame()
